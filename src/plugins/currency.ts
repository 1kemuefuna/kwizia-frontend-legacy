import Vue from 'vue'

const currency = (value: number)=>{
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'NGN',
        minimumFractionDigits: 0
    });
    return formatter.format(value);
}

Vue.filter('currency', function (number: number) {
    if (!number) return ''
    return currency(number);
})
