import CryptoJS from "crypto-js";
import {serialize, unserialize} from 'php-serialize'

export default class CryptoKwizia{
    key: string = "UBD6+avdn62G+yAO7+dmjIjcRBOg3nBnFrOi+4uIdiY=";

    decrypt(encryptStr: string){
        let setKey = encryptStr.substring(0, 32);
        let EncryptionString = encryptStr.substring(42);
        let encryptStrBase = CryptoJS.enc.Base64.parse(EncryptionString);
        let encryptData = encryptStrBase.toString(CryptoJS.enc.Utf8);
        encryptData = JSON.parse(encryptData);
        // @ts-ignore
        let iv = CryptoJS.enc.Base64.parse(encryptData.iv);
        // @ts-ignore
        var decrypted: any = CryptoJS.AES.decrypt(encryptData.value,  CryptoJS.enc.Utf8.parse(setKey), {
            iv : iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        decrypted = CryptoJS.enc.Utf8.stringify(decrypted);
        // console.log(decrypted)
        // return JSON.parse(unserialize(decrypted));
        return JSON.parse(unserialize(decrypted));
    }

    encrypt(data: any){
        let setKey = this.generateRand(32);
        let pad = this.generateRand(10);
        let iv: any = CryptoJS.lib.WordArray.random(16),
        key = CryptoJS.enc.Utf8.parse(setKey);
        let options = {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        };
        let encrypted: any = CryptoJS.AES.encrypt(data, key, options);
        encrypted = encrypted.toString();
        iv = CryptoJS.enc.Base64.stringify(iv);
        let result: any = {
            iv: iv,
            value: encrypted,
            mac: CryptoJS.HmacSHA256(iv + encrypted, key).toString()
        }
        result = JSON.stringify(result);
        result = CryptoJS.enc.Utf8.parse(result);
        // console.log(key);
        return setKey+pad+CryptoJS.enc.Base64.stringify(result);
    }

    generateRand(len: number): string{
        var text = "";

            var charset = "abcdefghjklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXZ";

            for( var i=0; i < len; i++ )
            text += charset.charAt(Math.floor(Math.random() * charset.length));

            return text;
    }
    
}
