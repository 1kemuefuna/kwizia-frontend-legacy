import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import '@/sass/style.scss'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './plugins/currency'
import './plugins/social'
import './plugins/decimal'
import './plugins/vuelidate'
import './plugins/toastr'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
