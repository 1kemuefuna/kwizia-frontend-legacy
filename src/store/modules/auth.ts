import { $axios } from "@/plugins/axios";

export default {
    namespaced: true,
    state: {
        token: localStorage.getItem("authified") || false,
        //isLoggedIn: false,
        user: null,
      },
      getters: {
        isLoggedIn: (state: any) => !!state.token,
        user: (state: any) => state.user,
      },
      mutations: {
        SET_AUTHENTICATED(state: any, token: any) {
          state.token = token;
        },
    
        SET_USER(state: any, value: any) {
          state.user = value;
        },
      },
      actions: {
        async login({ commit }: any, credentials: any) {
          await $axios.get("/sanctum/csrf-cookie");
          await $axios
            .post("/api/v1/user/auth/login", credentials)
            .then((response) => {
              localStorage.setItem("authified", "1");
              commit("SET_AUTHENTICATED", localStorage.getItem("authified"));
              commit("SET_USER", response.data);
            });
          // sessionStorage.ID =
        },
        async social_login({ commit }: any, data: any) {
          localStorage.setItem("authified", "1");
              commit("SET_AUTHENTICATED", localStorage.getItem("authified"));
              commit("SET_USER", data);
        },
        async logout({ commit }: any) {
          await $axios.post("/api/v1/user/auth/logout").then((resp) => {
            commit("SET_AUTHENTICATED", "");
            commit("SET_USER", null);
            localStorage.removeItem("authified");
          });
        },
    
        async me({ commit }: any) {
          return await $axios
            .get("/api/v1/user/profile/me")
            .then((response) => {
              if (response.data == null) {
                localStorage.removeItem("authified");
                commit("SET_AUTHENTICATED", "");
                commit("SET_USER", null);
              } else {
                localStorage.setItem("authified", "1");
                commit("SET_AUTHENTICATED", localStorage.getItem("authified"));
                commit("SET_USER", response.data);
              }
            })
            .catch((error) => {
              localStorage.removeItem("authified");
              commit("SET_AUTHENTICATED", "");
              commit("SET_USER", null);
            });
        },

        async set_user({ commit }: any, data: any){
          commit("SET_USER", data);
        }
      },
}